#!/bin/bash


VDIR=".virtual";

function setup {
  virtualenv .virtual && source .virtual/bin/activate && pip install -r req.txt
}

function run {
   source .virtual/bin/activate && jupyter-notebook
}


if [ -d "$VDIR" ]; then
   run;
else
   setup;
   run;
fi


